import csv
      
#Hashtags in einem Tweet finden
def findHashtags(originalTweet):
  hashtagList =[]
  tweet = originalTweet
  #so lange es noch Hashtags in dem Text gibt
  while tweet.find("#") != -1 :
    #schneide alles was wir nicht brauchen weg
    tweet = tweet[tweet.find("#"):]
    #finde das ende des Hashtags
    endIndex = 1 + findFirstNonAlphaNum(tweet[1:])
    #wenn nicht der letzte Hashtag
    if endIndex !=0  and endIndex!=1 and endIndex != len(tweet) :
      #den Hashtag speichern
      hashtag = tweet[:endIndex]
      #fuege den der Liste hinzu 
      hashtagList.append(hashtag)
      #schneide alles unnotige weg und weiderhole
      tweet = tweet[endIndex:]
    elif endIndex == 1:
      tweet = tweet[endIndex:]
      continue
    else:
      #letzter Hashtag, beende!
      hashtagList.append(tweet)
      break
      
  return hashtagList

#finde das erste nicht alpha-numerische Buchstabe
def findFirstNonAlphaNum(text):
  for index, char in enumerate(text):
    if not char.isalnum():
      return index
  #wenn nicht gefunden
  return -1

#Die Spalten, die wir nicht brauchen, sind:
#truncated, is_quote_status, source_url 
#Aussserdem wird eine neue Spalte erstellt mit den Hashtags
def manipulate(row):
    #Spalten loeschen
    #truncated
    row.pop(10)
    #source_url
    row.pop(9)
    #is_quote_satus
    row.pop(6)
    #Timestamp aendern, da die From in Postgresql ist "YYYY-MM-DD HH-MM-SS"
    row[4] = row[4].replace("T"," ")
    #Hashtags aus dem text herausfinden
    hashtagsInThisTweet = findHashtags(row[1])
    #Hashtags in eigener Spalte speichern
    #die Hashtags werden mit Komma getrennt
    row.append(','.join(hashtagsInThisTweet))
    return row
    
#Hauptfunktion
def main():
  #lese den Inhalt der Datei
  with open('american-election-tweets.csv','r') as readFile:
    fileReader = csv.reader(readFile, delimiter=';', quotechar='"')
    #breite fuer das Schreiben vor
    with open('american-election-tweets_CLEANED.csv','w') as writeFile:
      fileWriter = csv.writer(writeFile, delimiter=";", quotechar='|', quoting=csv.QUOTE_MINIMAL)
      #Anfang der Schleife
      firstRow = True
      for row in fileReader:
        #ignoriere die erste Zeile
        if firstRow:
          firstRow = False
          continue
        #bearbeite Daten  
        row = manipulate(row)
        #schreibe Daten in die Datei
        fileWriter.writerow(row)
  
#start
main()
  
  