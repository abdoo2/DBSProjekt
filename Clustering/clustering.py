import psycopg2
import matplotlib.pyplot as plt
import random
from aehnlichkeit import numericRepresentation, corners, keyHashtags
import scipy.cluster.vq as vq

def main():
  #Verbindung zur Datenbank erstellen
  conn_string = "dbname='election' user='testuser' password='testpass'"
  conn = psycopg2.connect(conn_string)
  conn.autocommit = True
  cursor = conn.cursor()

  #plot corners
  for i in range(len(corners)):
    plt.plot(corners[i][0],corners[i][1],'oc')
    plt.annotate(keyHashtags[i], (corners[i][0],corners[i][1]))
  #Hashtags aus der Datenbank holen und speichern
  cursor.execute("select inhalt from hashtag;")
  hashtagList = []
  results = cursor.fetchall()
  for item in results:
    hashtagList.append(item[0])
  
  #berechne die Koordinatenliste
  #random.seed(5)
  points = []
  for item in hashtagList:
    temp = numericRepresentation(item)
    points.append([temp[0],temp[1]])

  #k means berechnen
  centroids,idx = vq.kmeans2(points, 5)
  #idx,_ = vq.vq(points,centroids)
  
  #plot erstellen
  #colors
  colors = ['or','og','ob','oy','om','oc']
  #centroids
  plt.scatter(centroids[:,0],centroids[:,1], c = 'k', marker = 'x', s = 200)
  #points
  for i in range(len(points)):
    plt.plot(points[i][0],points[i][1],colors[idx[i]])
    #plt.annotate(hashtagList[i],(points[i][0],points[i][1]))
  plt.show()
  

  
main()