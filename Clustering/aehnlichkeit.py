corners = [[-100,-100],[-100,100],[100,-100],[100,100]]
keyHashtags = ["#trump2016", "#debate2016", "#imwithyou","#rncincle"]

import matplotlib.pyplot as plt
import numpy as np
from words import *

def numericRepresentation(hashtag):
  #Einstellungen
  keywords = [candidates,singleWords,extra]
  values   = [80,60,40]
  
  #Initialisierung
  x = 0
  y = 0
  
  #Schleife fuer die 4 wichtigsten Hashtags
  for cornerIndex in range(len(keyHashtags)):
    if hashtag == keyHashtags[cornerIndex]:
      x , y = movePoint(x,y,100, corners[cornerIndex])
    else:
      gleichheit = 0
      gefundeneGleichheit = 0
      
      h2 = keyHashtags[cornerIndex]
      h1 = hashtag
      
      #gehe Liste aller Woerter durch
      for index in range(len(keywords)):
        arrays = keywords[index]
        value  = values[index]
        
        #gehe alle arrays durch
        for words in arrays:
          for word in words:
            #wenn ein Wort in dem ersten Hashtag vorkommt
            if h1.find(word) != -1 : 
                for word2 in words:
                  #kommt ein aehnliches Wort in dem zweiten Hashtag vor?
                  if h2.find(word2) != -1 :
                    #wenn ja, erhoehe Gleichheit und loesche beide aus den Hashtags
                    gleichheit += value/np.power((value/10), gefundeneGleichheit)
                    gefundeneGleichheit+=1
                    h1 = h1.replace(word, '#')
                    h2 = h2.replace(word2, '#')

      #analog zu oben, dies Mal aber mit Charakter-Paaren
      for i in range(len(h1) - 2):
        temp = h1[i] + h1[i+1]
        if temp.find('#') != -1 :
          if h2.find(temp) != -1 :
            gleichheit += 50/np.power(5,gefundeneGleichheit)
            gefundeneGleichheit+=1
            h1 = h1.replace(temp, '#')
            h2 = h2.replace(temp, '#')

      #vergleiche die Laengen der beiden Hashtags
      lengthes = len(hashtag)/len(keyHashtags[cornerIndex])
      
      if lengthes <= 1 :
        gleichheit += (lengthes- 0.3) *7.5
      else :
        gleichheit += ((1/lengthes)- 0.3) *7.5
      
      gleichheit =  np.maximum(0,np.minimum(gleichheit,100))
      
      x , y = movePoint(x,y,gleichheit, corners[cornerIndex])
    
  return x,y

def movePoint(x, y, percent, target):
  abstand = percent * np.sqrt(2)
  winkel = np.arctan(np.absolute(target[1] - y)/np.absolute(target[0]-x))
  if(target[0] >= x and target[1] >= y):
    resulty = y +np.sin(winkel)*abstand
    resultx = x +np.cos(winkel)*abstand
  elif (target[0] >= x and target[1] < y):
    resulty = y -np.sin(winkel)*abstand
    resultx = x +np.cos(winkel)*abstand
  elif (target[0] < x and target[1] < y):
    resulty = y -np.sin(winkel)*abstand
    resultx = x -np.cos(winkel)*abstand
  else:
    resulty = y +np.sin(winkel)*abstand
    resultx = x -np.cos(winkel)*abstand
  #print(resultx,resulty)
  return resultx, resulty
