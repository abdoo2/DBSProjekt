import random

corners = [[0,200],[200,0],[0,0],[200,200]]
keyHashtags = ["#trump2016", "#debatenight", ""]

#turn a string into a point using basic rules about the content of the string
def numericRepresentation(hashtag):
  #using random values to make the plot more distributed
  #trump        +1 x axis
  #hillary      -1 x axis
  #neutral      +1 y
  #controversial-1 y
  
  #start values
  x   = random.uniform(-0.5, 0.5) 
  y   = random.uniform(-0.5, 0.5)
  
  #Berechnungen
  x += calculate(hashtag, trump,1)
  x -= calculate(hashtag, hillary,1)
  
  y += calculate(hashtag, neutral, 1)
  y -= calculate(hashtag, controversial, 1)
  
  #Ausgabe
  return (x,y)
  
##########################################
def calculate(hashtag, array, value):
  for word in array:
    if hashtag.find(word) != -1:
      return random.uniform(value - 0.15, value + 0.15)
  return 0