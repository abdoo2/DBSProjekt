from flask import Flask
from flask_cors import CORS, cross_origin
import psycopg2
app = Flask(__name__)
CORS(app)
#verbindung Eigenschaften
conn_string = "dbname='election' user='testuser' password='testpass'"
#verbinden
conn = psycopg2.connect(conn_string)
#conn.autocommit = True
cursor = conn.cursor()
    
@app.route("/gemeinsamAuftreten", methods=['GET', 'POST'])
def gemeinsamAuftreten():
  #alle hashtag paaren
  cursor.execute('select a.inhalt ,b.inhalt,hashtag_pairs.haeufigkeit from hashtag as a, hashtag as b,\
  hashtag_pairs where a.hashtagid=firsthashtagid and b.hashtagid=SECONDHASHTAGID');
  temp = cursor.fetchall()
  result = ""
  for row in temp:
    for item in row:
        result += str(item) + ","
  #restlichen hashtags
  cursor.execute('select inhalt,haeufigkeit from hashtag;')
  temp = cursor.fetchall()
  for row in temp:
    result += row[0] + "_" + str(row[1]) + "-"
  return result
  
@app.route("/allHashtags", methods=['GET', 'POST'])
def allHashtags():
  cursor.execute('select inhalt,haeufigkeit from hashtag;')
  result =""
  for row in cursor.fetchall():
    result += row[0] +"," + str(row[1]) + "_"
  return result


@app.route("/getHashtagsWithTime", methods=['GET', 'POST'])
def hashtagsWithTime():
  cursor.execute("select hashtagid,inhalt from hashtag;")
  result = ""
  hashtagIDS = []
  rows = cursor.fetchall()
  for row in rows:
    id     = str(row[0])
    inhalt = row[1]
    result += inhalt+","
    cursor.execute('select zeit from (select zeit, tweetid from tweet) as a, tweet_hashtag\
    where hashtagid = %s and a.tweetid = tweet_hashtag.tweetid',(id,));
    times = cursor.fetchall()
    for time in times:
      res = str(time)
      res = res.split(",")[0][-4:] + res.split(",")[1]+ res.split(",")[2]
      result += res +","
    result += "_"
  
  return result