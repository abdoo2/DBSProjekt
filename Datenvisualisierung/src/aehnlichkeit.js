function berechneGleichheit(ersterHashtag, zweiterHashtag)  {
  //Einstellungen
  const keywords = [candidates,singleWords,extra]
  const values   = [80,60,40]
  
  //Initialisierung
  h1 = ersterHashtag
  h2 = zweiterHashtag
  gleichheit = 0
  gefundeneGleichheit = 0
  
  //gehe Liste aller Woerter durch
  for (var i = 0; i< values.length; i++) {
    arrays = keywords[i]
    value  = values[i]
    
    //gehe alle arrays durch
    for(words in arrays){
      for (index in arrays[words]){
        word = arrays[words][index]
        
        //wenn ein Wort in dem ersten Hashtag vorkommt
        if (h1.indexOf(word) != -1){
            for (index2 in arrays[words]){
              word2 = arrays[words][index2]
              
              //kommt ein aehnliches Wort in dem zweiten Hashtag vor?
              if (h2.indexOf(word2) != -1){
                  
                //wenn ja, erhöhe Gleichheit und lösche beide aus den Hashtags
                gleichheit += value/Math.pow((value/10), gefundeneGleichheit)
                gefundeneGleichheit++
                h1 = h1.replace(word, '#')
                h2 = h2.replace(word2, '#')
              }
            }
          }
      }
    }
  }

  //analog zu oben, dies Mal aber mit Charakter-Paaren
  for (var i =0; i < h1.length-1; i++){
    temp = h1[i] + h1[i+1];
    if(temp.indexOf('#') != -1){
      if(h2.indexOf(temp) != -1){
        gleichheit += 50/Math.pow(5,gefundeneGleichheit)
        gefundeneGleichheit++
        h1 = h1.replace(temp, '#')
        h2 = h2.replace(temp, '#')
      }
    }
  }

  //vergleiche die Längen der beiden Hashtags
  lengthes =(ersterHashtag.length/zweiterHashtag.length)
  
  if(lengthes <= 1)
    gleichheit += (lengthes- 0.3) *7.5
  else
    gleichheit += ((1/lengthes)- 0.3) *7.5
  
  //console.log(gleichheit)

  return Math.max(0,Math.min(gleichheit,100));

}