function gemeinsamAuftreten() {
  $.ajax({
      type: "POST",
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'text/plain'},
      url: "http://localhost:5000/gemeinsamAuftreten",
      data: {},
      success: drawGemeinsamAuftreten
  });
}


function drawGemeinsamAuftreten(response) {
  //Einstellungen
  const BASE_NODE_SIZE         = 0.5
  const NODE_SIZE_SCALE_FACTOR = 1/30
  const NODE_COLOR             = "#f00"
  const EDGE_COLOR             = "#ccc"
  //
  //    EDGE_SIZE              = wird nach der Haeufigkeit berechnet !
  
  //respone ist ein String und beinhaltet alle Hatshtags mit ihren Haefigkeiten als Paare
  //response hat den Form: "#foo,#foo2,x,#foo3,#foo4,x2..."
  //result ist ein Array von Hashtag-Hashtag-Haeufigkeit Strings
  //result hat den Form: ["#foo","#foo2","x",...]
  result = response.split(",")
  extraHashtags = result.pop()
  
  //Graph Elemente erstellen
  var graphElements = {
    nodes: [],
    edges: []
  };
  
  //Knoten/Kanten erstellen
  hashtags = []
  var i = 0
  while (i < result.length){
    //Elemente extrahieren
    firstHashtag  = result[i]
    secondHashtag = result[i+1]
    thickness     = result[i+2]
    
    //hier werden die Hashtags und ihre Indizes gespeichert,
    //um eine Kante zwischen den beiden zu erstellen
    currentHashtags = [firstHashtag, secondHashtag]
    currentIndices  = []
    
    currentHashtags.forEach(function(tag){
      //finde Index des Hashtags
      ind = (hashtags.indexOf(tag))
      //neuer Hashtag? addiere zu Liste !
      if (ind === -1)
      {
        ind = hashtags.length
        currentIndices.push(ind)
        hashtags.push(tag)
        graphElements.nodes.push({
          id: 'n' + ind,
          label: tag,
          x: Math.random()*2.5,
          y: Math.random(),
          color: NODE_COLOR
        });
      }
      else
        currentIndices.push(ind)
    }); 
    //addiere eine Kante
    graphElements.edges.push({
      id: 'e' + i,
      source: 'n' + currentIndices.pop(),
      target: 'n' + currentIndices.pop(),
      size: thickness,
      color: EDGE_COLOR
    });
    i+=6
  }
  
  //zustzliche hashtags addieren
  extraHashtags = extraHashtags.split("-")
  
  for (var k = 0; k < extraHashtags.length; k ++ ){
    //Elemente extrahieren
    hashtag = extraHashtags[k].split("_")[0]
    freq    = parseInt(extraHashtags[k].split("_")[1])
    
    //neuer Hashtag? addiere zur Liste !
    if (hashtags.indexOf(hashtag) === -1){
      ind = hashtags.length
      hashtags.push(hashtag)
      graphElements.nodes.push({
        id: 'n' + ind,
        label: hashtag,
        x: Math.random()*2,
        y: Math.random(),
        size: BASE_NODE_SIZE + freq * NODE_SIZE_SCALE_FACTOR,
        color: NODE_COLOR
        });
    }
    //bestimme die Groesse nach der Haeufigkeit
    else{
      graphElements.nodes[hashtags.indexOf(hashtag)]
      .size = BASE_NODE_SIZE + freq * NODE_SIZE_SCALE_FACTOR
    }
  }
  // Instantiate sigma:
  var s = new sigma({
    graph: graphElements,
    drawLabels: false,
    hideEdgesOnMove: true,
    scalingMode: "outside",
    container: 'gemeinsamAuftreten-container'
  });

}