function aehnlichkeitsVisiualisierung() {
    $.ajax({
        type: "POST",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'text/plain'},
        url: "http://localhost:5000/allHashtags",
        data: {},
        success: drawAehnlichkeitsVisiualisierung
    });
}

function drawAehnlichkeitsVisiualisierung(response){
  //Einstellungen !!
  const MIN_FREQUENCY  = 2
  const NODE_SIZE      = 100
  const NODE_COLOR     = "#f00"
  const EDGE_SIZE      = 30
  const MIN_SIMILARITY = 50
  const SIMILARITIES   = [90     ,75     ,50    ]
  const COLORS         = ["#f00" ,"#faa" ,"#eef"]
  
  //respone ist ein String und beinhaltet alle Hatshtags mit ihren Haefigkeiten als Paare
  //response hat den Form: "#foo,x_#foo2,x2..."
  //result ist ein Array von Hashtag-Haeufigkeit Strings
  //result hat den Form: ["#foo,x","#foo2,x2,...]
  result  = response.split("_")
  result.pop()
  
  //Graph Elemente erstellen
  var graphElements = {
    nodes: [],
    edges: []
  };
  
  //Knoten
  for (var k = 0; k<result.length; k++){
    //Informationen extrahieren
    hashtag = result[k].split(",")[0]
    freq    = result[k].split(",")[1]
    //Knoten werden nur addiert, wenn sie mehr als zwei mal vorkammen
    if(freq > MIN_FREQUENCY)
      graphElements.nodes.push({
        id: 'n' + k,
        label: hashtag,
        x: Math.random()*2.5,
        y: Math.random(),
        size: NODE_SIZE,
        color: NODE_COLOR
      });
    }
  
  //Kanten
  for(var k = 0; k <result.length; k++){
    for(var l = 0; l < result.length; l++){
      
      //Informationen extrahieren
      firstHashtag  = result[k].split(",")[0]
      firstFreq     = result[k].split(",")[1]
      secondHashtag = result[l].split(",")[0]
      secondFreq    = result[l].split(",")[1]
      
      //sim ist die Aehnlichkeit der beiden Hahstags
      sim = berechneGleichheit(firstHashtag, secondHashtag)
      
      //col ist die Farbe der Kante, wird nach der Aehnlichkeit bestimmt
      col = ""
      SIMILARITIES.some(function(value, index){
        if (sim > value){
          col = COLORS[index]
          return true
        }  
      })
      
      //Kante wird nur hinzugefuegt, wenn die Aehnlichkeit greosser als MIN_SIMILARITY
      //und beide Konten haben eine Hauefigkeit groesser als MIN_FREQUENCY
      if (sim > MIN_SIMILARITY && firstFreq > MIN_FREQUENCY && secondFreq > MIN_FREQUENCY){
        graphElements.edges.push({
        id: (k*1000)+(l),
        source: 'n' + k,
        target: 'n' + l,
        size: EDGE_SIZE,
        color: col
        });
      }
    } 
  }
    
  //erstelle das Diagramm   
  var s = new sigma({
    graph: graphElements,
    hideEdgesOnMove: true,
    drawLabels: false,
    scalingMode: "outside",
    container: 'aehnlichkeits-container'
  });
  console.log("feritg")
   
}
