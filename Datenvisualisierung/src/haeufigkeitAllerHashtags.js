function auftretenAllerHashtags() {
    $.ajax({
        type: "POST",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'text/plain'},
        url: "http://localhost:5000/getHashtagsWithTime",
        data: {},
        success: drawAllHashtags
    });
}


hashtags = []
infos    = []
everything = []

function drawAllHashtags(response){
  //Einstellungen
  const showAll   = false
  const MIN_ITEMS = 20
  
  //request bearbeiten
  result = response.split(",_")
  //absteigend sortieren
  result.sort(function(a, b){
      return b.length - a.length;
    }); 
  for (var k =0; k < result.length; k++){
    //Informationen extrahieren
    hashtag = result[k].split(",")[0]
    times   = result[k].split(",").slice(1)
    
    //Haeufigkeit und Zeiten berechnen
    freq = []
    date = []
    last = ""
    for(var i = 0; i < times.length; i++){
      //wurde den Hashtag im selben Tag benutzt?
      if (last == times[i]){
        //erhohe die Haeufigkeit
        freq[freq.length-1]++
        last = times[i]
        times.splice(i, 1);
        i--
      }
      else{
        //zaehle die Haeufigkeit fuer naechsten Tagen
        date.push(convertTime(times[i]))
        freq[freq.length] = 1
        last = times[i]
      }
    }
    
    everything.push({label:hashtag, data: zip([date, freq])})
    hashtags.push(hashtag)
    infos.push(zip([date, freq]))
  }
  
  plotAll(true)
}

function plotAll(minimum){
  settings = 
  {
    xaxis:{mode: "time",timeformat: "%d/%m"},
    series: {
      bars:{show: true, align: "center",   lineWidth: 0, barWidth: 1000*60*60*24},
      stack: true
      },
    legend: {show: false, noColumns: 30, margin: 15,position: "nw"},
    grid: {
        hoverable: true,
        borderWidth: 0,        
        backgroundColor: { colors: ["#EDF5FF", "#ffffff"] }
    }
  }
  
  if (minimum){
    $('#container').css('width', '1300px');
    $('#legend').hide();
    settings.legend.show = false;
  }
  else{
    settings.legend.container = "#legend";
    $('#container').css('width', '4000px');
    $('#legend').show();
    settings.legend.show = true;
  }
    
  //Plot erstellen
  //Bar-Chart-Grafik
  $.plot("#container", everything, settings);
  
}

function drawHashtag(){
  tag = $('#tag').val();
  ind = hashtags.indexOf(tag)
  if(ind === -1)
    $('#tag').val("#nicht gefunden");
  else 
    $.plot(
      "#container2",
      [infos[ind]],
      {
        xaxis:{mode: "time",timeformat: "%d/%m"},
        series: {
          bars:{show: true, align: "center",   lineWidth: 1, barWidth: 1000*60*60*24}
          },
        grid: {
            hoverable: true,
            borderWidth: 0,        
            backgroundColor: { colors: ["#EDF5FF", "#ffffff"] }
        }
      });
}


//zip funktion, um Tupeln aus den Daten zu erstellen
function zip(arrays) {
    return arrays[0].map(function(_,i){
        return arrays.map(function(array){return array[i]})
    });
};

//die Funktion bekommt ein String und gibt Date zurück
function convertTime(date) {
  var dateParts = date.split(" ");
  return (new Date(parseInt(dateParts[0]),parseInt(dateParts[1]) - 1, parseInt(dateParts[2])));
  // month ist 0-basiert
}
