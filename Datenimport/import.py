import psycopg2
import sys
import csv
 
def main():
  #verbindung Eigenschaften
  conn_string = "dbname='election' user='testuser' password='testpass'"
  print ("Connecting to database\n	->%s" % (conn_string))
  #verbinden
  conn = psycopg2.connect(conn_string)
  conn.autocommit = True
  cursor = conn.cursor()
  print ("Connected!\n")
  #lese den Inhalt der Datei
  with open('american-election-tweets_CLEANED.csv','r') as readFile:
    fileReader = csv.reader(readFile, delimiter=';', quotechar='|')
    for row in fileReader:
      #print(row)
      #Elements extrahieren
      handle = row[0]
      inhalt = row[1]
      is_retweet = row[2]
      original_author = row[3]
      time = row[4]
      in_reply_to_screen_name = row[5]
      retweet_count = row[6]
      favorite_count = row[7]
      hashtags = row[8]
      #handle
      cursor.execute('INSERT INTO handle(benutzername) values(%s) ON CONFLICT (benutzername) DO\
        UPDATE SET benutzername =%s RETURNING handleid',(handle,handle,))
      rows = cursor.fetchall()
      handleid = rows[0][0]
      #tweets
      cursor.execute('INSERT INTO tweet(inhalt, zeit, retweet_count, favorite_count) values \
      (%s,%s,%s,%s) RETURNING tweetid',(inhalt,time,retweet_count,favorite_count,))
      rows = cursor.fetchall()
      tweetid = rows[0][0]
      #handle_tweet
      cursor.execute('INSERT INTO handle_tweet values (%s,%s,%s,%s,%s)',
      (handleid,tweetid,is_retweet,original_author,in_reply_to_screen_name,))
      #cursor.fetchall()
      #hashtags
      hashtagidList = []
      if hashtags != '':
          for tag in hashtags.split(','):
            if tag != '' and tag != '"':
                #print(tag)
                cursor.execute('INSERT INTO hashtag(inhalt, haeufigkeit) values(%s,%s) ON CONFLICT (inhalt)\
                DO UPDATE SET haeufigkeit = hashtag.haeufigkeit +1 RETURNING hashtag.hashtagid;',(tag,1,))
                rows = cursor.fetchall()
                hashtagidList.append(rows[0][0])
      #tweet_hashtag
      if hashtagidList:
          for tagid in hashtagidList:
            cursor.execute('INSERT INTO tweet_hashtag values (%s,%s) ON CONFLICT DO NOTHING',(tweetid,tagid,))
            #cursor.fetchall()
      #hashtag paare
      if hashtagidList:
          for i in range(len(hashtagidList)):
            for k in range(len(hashtagidList)):
              if (k == i):
                continue
              cursor.execute('INSERT INTO hashtag_pairs values (%s,%s,1) ON CONFLICT (firsthashtagid, secondhashtagid)\
              DO UPDATE SET haeufigkeit = hashtag_pairs.haeufigkeit +1 ',(hashtagidList[i],hashtagidList[k],))
              #cursor.fetchall()
              
  cursor.close()
  conn.close()
  
 
if __name__ == "__main__":
	main()