CREATE TABLE handle (
    handleid SERIAL PRIMARY KEY,
    benutzername varchar(15) NOT NULL UNIQUE
);

CREATE TABLE tweet (
    tweetID SERIAL PRIMARY KEY,
    inhalt varchar(165),
    zeit Timestamp NOT NULL,
    favorite_count Int NOT NULL,
    retweet_count Int NOT NULL
);

CREATE TABLE hashtag (
    hashtagid SERIAL PRIMARY KEY,
    inhalt varchar(165) NOT NULL UNIQUE,
    haeufigkeit Int NOT NULL
);

CREATE TABLE handle_tweet (
    handleid Int REFERENCES handle(handleid),
    tweetid Int REFERENCES tweet(tweetid),
    is_retweet Bool,
    original_author varchar(15),
    in_reply_to_screen_name varchar(15),
    PRIMARY KEY (handleid, tweetid)
);

CREATE TABLE tweet_hashtag (
    tweetid Int REFERENCES tweet(tweetid),
    hashtagid Int REFERENCES hashtag(hashtagid),
    PRIMARY KEY (tweetid, hashtagid)
);

CREATE TABLE hashtag_pairs (
    firsthashtagid Int REFERENCES hashtag(hashtagid),
    secondhashtagid Int REFERENCES hashtag(hashtagid),
    haeufigkeit Int NOT NULL,
    PRIMARY KEY (firsthashtagid, secondhashtagid)
);